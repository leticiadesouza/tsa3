#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


#define TAM_VETOR 3323

struct Cadastro{
    char uf[5];
    char municipio[50];
    int CD;
    int EPAO;
    int TPD;
    int LB;
    int TSB;
    int ASB;
    int APD;
    int EPO;
    int total;
};

struct Cadastro cadastro[TAM_VETOR];

typedef struct arvore {
    struct Cadastro item;
    struct arvore * esquerda;
    struct arvore * direita;
} Arvore;

Arvore *cria_arvore(){
    return NULL;
}

void inserir_mun(Arvore ** arv, struct Cadastro item){
    if (*arv == NULL){
        *arv = (Arvore*)malloc(sizeof(Arvore));
        (*arv)->item = item;
        (*arv)->esquerda = NULL;
        (*arv)->direita = NULL;
    }else {
        if (strcmp(item.municipio, (*arv)->item.municipio)<0){/* Se o item vier antes vai para a esquerda*/
            inserir_mun(& (*arv)->esquerda, item);
        }
        if (strcmp(item.municipio, (*arv)->item.municipio)>0){
            inserir_mun(& (*arv)->direita, item);
        }
    }

}

void inserir_CD(Arvore ** arv, struct Cadastro item){
    if (*arv == NULL){
        *arv = (Arvore*)malloc(sizeof(Arvore));
        (*arv)->item = item;
        (*arv)->esquerda = NULL;
        (*arv)->direita = NULL;
    }else {
        if (item.CD < (*arv)->item.CD){/* Se o item vier antes vai para a esquerda*/
            inserir_CD(&(*arv)->esquerda, item);
        }
        if (item.CD > (*arv)->item.CD){
            inserir_CD(&(*arv)->direita, item);
        }
    }

}

void inserir_total(Arvore ** arv, struct Cadastro item){
    if (*arv == NULL){
        *arv = (Arvore*)malloc(sizeof(Arvore));
        (*arv)->item = item;
        (*arv)->esquerda = NULL;
        (*arv)->direita = NULL;
    }else {
        if (item.total<(*arv)->item.total){/* Se o item vier antes vai para a esquerda*/
            inserir_total(&(*arv)->esquerda, item);
        }
        if (item.total>(*arv)->item.total){
            inserir_total(&(*arv)->direita, item);
        }
    }

}


void mostrar_arvore(Arvore* a){
    if(a != NULL){
        mostrar_arvore(a->esquerda);
        printf("-------------------------------------------\n");
        printf("UF = %s\n", a->item.uf);
        printf("Municipio = %s\n", a->item.municipio);
        printf("CD = %d\n", a->item.CD);
        printf("EPAO = %d\n", a->item.EPAO);   
        printf("TPD = %d\n", a->item.TPD);
        printf("LB = %d\n", a->item.LB);
        printf("TSB = %d\n", a->item.TSB);
        printf("ASB = %d\n", a->item.ASB);
        printf("APD = %d\n", a->item.APD);
        printf("EPO = %d\n", a->item.EPO);
        printf("total = %d\n", a->item.total);
        mostrar_arvore(a->direita);
            
    }
}

void busca_estado(Arvore* a, char *nome){
    if (a != NULL){
        busca_estado(a->direita, nome);//olhando primeiro pra direita
        if(strcmp(a->item.uf, nome) == 0){
            printf("-------------------------------------------\n");
            printf("UF = %s\n", a->item.uf);
            printf("Municipio = %s\n", a->item.municipio);
            printf("CD = %d\n", a->item.CD);
            printf("EPAO = %d\n", a->item.EPAO);   
            printf("TPD = %d\n", a->item.TPD);
            printf("LB = %d\n", a->item.LB);
            printf("TSB = %d\n", a->item.TSB);
            printf("ASB = %d\n", a->item.ASB);
            printf("APD = %d\n", a->item.APD);
            printf("EPO = %d\n", a->item.EPO);
            printf("total = %d\n", a->item.total);
        }
        busca_estado(a->esquerda, nome);
    }
}

void busca_municipio(Arvore* a, char *nome){
    if (a != NULL){
        if(strcmp(a->item.municipio, nome) == 0){
            printf("-------------------------------------------\n");
            printf("UF = %s\n", a->item.uf);
            printf("Municipio = %s\n", a->item.municipio);
            printf("CD = %d\n", a->item.CD);
            printf("EPAO = %d\n", a->item.EPAO);   
            printf("TPD = %d\n", a->item.TPD);
            printf("LB = %d\n", a->item.LB);
            printf("TSB = %d\n", a->item.TSB);
            printf("ASB = %d\n", a->item.ASB);
            printf("APD = %d\n", a->item.APD);
            printf("EPO = %d\n", a->item.EPO);
            printf("total = %d\n", a->item.total);
        }else{
            if(strcmp(nome, a->item.municipio)<0){
                busca_municipio(a->esquerda, nome); 
            }
            if (strcmp(nome, a->item.municipio)>0){
                busca_municipio(a->direita, nome);
            }
        }
    }
}

void arvore_destrutor(Arvore *raiz){
    if(raiz!=NULL){
        arvore_destrutor(raiz->esquerda);
        arvore_destrutor(raiz->direita);
        free(raiz);
        raiz=NULL;
    }
}