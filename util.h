#include "arvore.h"

#define TAM_VETOR 3323

struct Cadastro cadastro[TAM_VETOR];

int mostrar_menu(){
  int opcao;
  system ("clear");

  printf("********************************************************\n");
  printf("*              Universidade de Brasilia                *\n");
  printf("*     Disciplina: Estrutura de Dados e Algoritmos      *\n");
  printf("*     Aluna: Leticia de Souza Santos - 15/0015160      *\n");
  printf("********************************************************\n");
  printf("*          Menu de consulta ao cadastro do CFO         *\n");
  printf("* Selecione a opção desejada:                          *\n");
  printf("* 1- Importar dados                                    *\n");
  printf("* 2- Visualizar todos os relatorios                    *\n");
  printf("* 3- Visualizar relatorio de um estado                 *\n");
  printf("* 4- Consultar dados de um Municipio                   *\n");
  printf("* 5- Sair do sistema                                   *\n");
  printf("********************************************************\n");

scanf("%d", &opcao);

  while (opcao <= 0 || opcao >5) {
    printf("Opcao invalida! Selecione uma opcao entre 1 e 5.\n");
    scanf("%d", &opcao);
  }
  return opcao;
}

int mostrar_menu_relatorios(){
    int opcao;
    system ("clear");
    printf("*********************************************************\n");
    printf("*                       RELATORIOS                      *\n");
    printf("*********************************************************\n");
    printf("*       De que modo deseja organizar os relatorios:     *\n");
    printf("* 1- Municipio                                          *\n");
    printf("* 2- CD                                                 *\n");
    printf("* 3- Total                                              *\n");
    printf("* 4- Voltar ao Menu Principal                           *\n");
    printf("*********************************************************\n");

    scanf("%d", &opcao);
    while (opcao <= 0 || opcao >4) {
        printf("Opcao invalida! Selecione uma opcao entre 1 e 4.\n");
        scanf("%d", &opcao);
    }
    return opcao;
}


void relatorio_uf(Arvore *arv){
    char * UF;
    UF = malloc(sizeof (char*));
    printf("Digite a UF desejada:\n");
    setbuf(stdin, NULL);
    scanf("%[A-Z a-z]", UF);
    busca_estado(arv, UF);
    setbuf(stdin, NULL);
    printf("Pressione qualquer tecla para continuar...");
    getchar();
}

void relatorio_especifico(Arvore *arv){
    char * municipio;
    municipio = malloc(sizeof (char*));
    printf("Digite o nome do municipio desejado:\n");
    setbuf(stdin, NULL);
    scanf("%[A-Z a-z]", municipio);
    busca_municipio(arv, municipio);
    setbuf(stdin, NULL);
    printf("Pressione qualquer tecla para continuar...");
    getchar();
}



void importar() {
    FILE* fp = fopen("CFO_UF_municipios_brasil.csv", "r");
    char *info;
    char aux[100];
    int i;
    if(fp == NULL){
        printf("Erro ao abrir o arquivo\n");
    }else{
        for(i = 0; i < TAM_VETOR; i++) {
            printf("cont = %d\n", i);
            fgets(aux,100,fp);
            aux[strlen(aux)-1] = '\0';
            info = strtok(aux, ";");
            strcpy(cadastro[i].uf, info);
            info = strtok(NULL, ";");
            strcpy(cadastro[i].municipio, info);
            info = strtok(NULL, ";");
            cadastro[i].CD = atoi(info);
            info = strtok(NULL, ";");
            cadastro[i].EPAO = atoi(info);
            info = strtok(NULL, ";");
            cadastro[i].TPD = atoi(info);
            info = strtok(NULL, ";");
            cadastro[i].LB = atoi(info);
            info = strtok(NULL, ";");
            cadastro[i].TSB = atoi(info);
            info = strtok(NULL, ";");
            cadastro[i].ASB = atoi(info);
            info = strtok(NULL, ";");
            cadastro[i].APD = atoi(info);
            info = strtok(NULL, ";");
            cadastro[i].EPO = atoi(info);
            info = strtok(NULL, ";");
            cadastro[i].total = atoi(info);
        }
    }
    fclose(fp);
    //printf("Dados importados com sucesso!\n Pressione qualquer tecla para continuar");
    //fflush(stdio);
    //getch();
}