void importar() {
    FILE* fp = fopen("dadoscfo.csv", "r");
    char *pch;
    char aux[100];
    int i;
    int parametro = 0;
    if(fp == NULL){
        printf("Erro ao abrir o arquivo\n");
        parametro = 1;
    }

    if(parametro == 0) {
        for(i = 0; i < TAM_VETOR; i++) {
            fgets(aux,100,fp);
            aux[strlen(aux)-1] = '\0';
            pch = strtok(aux, ";");
            strcpy(cadastro[i].uf, pch);
            pch = strtok(NULL, ";");
            strcpy(cadastro[i].municipio, pch);
            pch = strtok(NULL, ";");
            cadastro[i].CD = atoi(pch);
            pch = strtok(NULL, ";");
            cadastro[i].EPAO = atoi(pch);
            pch = strtok(NULL, ";");
            cadastro[i].TPD = atoi(pch);
            pch = strtok(NULL, ";");
            cadastro[i].LB = atoi(pch);
            pch = strtok(NULL, ";");
            cadastro[i].TSB = atoi(pch);
            pch = strtok(NULL, ";");
            cadastro[i].ASB = atoi(pch);
            pch = strtok(NULL, ";");
            cadastro[i].APD = atoi(pch);
            pch = strtok(NULL, ";");
            cadastro[i].EPO = atoi(pch);
            pch = strtok(NULL, ";");
            cadastro[i].total = atoi(pch);
        }
    }
    fclose(fp);
}
