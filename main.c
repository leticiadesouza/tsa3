#include "util.h"

int main() {
    Arvore *arvore_mun = cria_arvore();
    Arvore *arvore_CD = cria_arvore();
    Arvore *arvore_total = cria_arvore();


    int opcao;
    int opcao2;
    int i;

    do {
        opcao = mostrar_menu();
        switch (opcao){
            case 1: 
                importar();
                for (i = 0; i < TAM_VETOR; ++i){
                    inserir_mun(&arvore_mun, cadastro[i]);
                }
                for (i = 0; i < TAM_VETOR; ++i){
                    inserir_CD(&arvore_CD, cadastro[i]);
                }
                for (i = 0; i < TAM_VETOR; ++i){
                    inserir_total(&arvore_total, cadastro[i]);
                }
                break;
            case 2: 
                opcao2 = mostrar_menu_relatorios();
                switch(opcao2){
                    case 1:
                        mostrar_arvore(arvore_mun);
                        setbuf(stdin, NULL);
                        printf("Pressione qualquer tecla para continuar...");
                        getchar();
                        break;
                    case 2: 
                        mostrar_arvore(arvore_CD);
                        setbuf(stdin, NULL);
                        printf("Pressione qualquer tecla para continuar...");
                        getchar();
                        break;
                    case 3:
                        mostrar_arvore(arvore_total);
                        setbuf(stdin, NULL);
                        printf("Pressione qualquer tecla para continuar...");
                        getchar();
                        break;
                    case 4:
                        //return;
                        break;
                }
                break;
            case 3:
                relatorio_uf(arvore_total);
                break;
            case 4:
                relatorio_especifico(arvore_mun);
                break;
            case 5:
                printf("Fim do programa\n");
                exit(0);
        }
        
    }while (opcao != 5);

    //limpando dados da memória
    arvore_destrutor(arvore_mun);
    arvore_destrutor(arvore_CD);
    arvore_destrutor(arvore_total);

    return 0;
}


